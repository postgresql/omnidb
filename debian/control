Source: omnidb
Section: database
Priority: optional
Maintainer: Debian PostgreSQL Maintainers <team+postgresql@tracker.debian.org>
Uploaders:
 Christoph Berg <myon@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 fonts-glyphicons-halflings | fonts-glewlwyd,
 fonts-roboto-unhinted,
 libjs-jquery,
 libjs-jquery-ui,
 python3:any | python3-all:any | python3-dev:any | python3-all-dev:any,
 python3:any | python3-all:any | python3-dev:any | python3-all-dev:any | dh-sequence-python3,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://omnidb.org/
Vcs-Browser: https://salsa.debian.org/postgresql/omnidb
Vcs-Git: https://salsa.debian.org/postgresql/omnidb.git

Package: omnidb-common
Architecture: all
Depends:
 ${fonts:Depends},
 ${js:Depends},
 ${misc:Depends},
 ${python3:Depends},
 python3-cherrypy3,
 python3-django (>= 2:2.2),
 python3-django-auth-ldap,
 python3-django-sass,
 python3-openpyxl,
 python3-paramiko,
 python3-pgspecial,
 python3-prettytable,
 python3-psycopg2,
 python3-pyaes,
 python3-pymysql,
 python3-restrictedpython,
 python3-scrypt,
 python3-social-django,
 python3-sqlparse,
 python3-sshtunnel,
Description: Web tool for database management (shared files)
 OmniDB is a web tool that simplifies database management focusing on
 interactivity, designed to be powerful and lightweight. Check-out some
 characteristics:
 .
  * Web Tool: Accessible from any platform, using a browser as a medium
  * Responsive Interface: All available functions in a single page
  * Unified Workspace: Different technologies managed in a single workspace
  * Simplified Editing: Easy to add and remove connections
  * Safety: Multi-user support with encrypted personal information
  * Interactive Tables: All functionalities use interactive tables, allowing
    copying and pasting in blocks
  * Smart SQL Editor: Contextual SQL code completion
  * Beautiful SQL Editor: You can choose between many available color themes
  * Tabbed SQL Editor: Easily add, rename or delete editor tabs
 .
 Supported DBMS: PostgreSQL, Oracle, MySQL, MariaDB
 .
 This package contains shared files.

Package: omnidb-server
Architecture: all
Depends:
 ${misc:Depends},
 adduser,
 omnidb-common (= ${binary:Version}),
Description: Web tool for database management
 OmniDB is a web tool that simplifies database management focusing on
 interactivity, designed to be powerful and lightweight. Check-out some
 characteristics:
 .
  * Web Tool: Accessible from any platform, using a browser as a medium
  * Responsive Interface: All available functions in a single page
  * Unified Workspace: Different technologies managed in a single workspace
  * Simplified Editing: Easy to add and remove connections
  * Safety: Multi-user support with encrypted personal information
  * Interactive Tables: All functionalities use interactive tables, allowing
    copying and pasting in blocks
  * Smart SQL Editor: Contextual SQL code completion
  * Beautiful SQL Editor: You can choose between many available color themes
  * Tabbed SQL Editor: Easily add, rename or delete editor tabs
 .
 Supported DBMS: PostgreSQL, Oracle, MySQL, MariaDB
 .
 This package runs OmniDB as system service on http://127.0.0.1:8000.
